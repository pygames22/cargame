import pygame
import random

# initialization
pygame.init()
# set display area
size = width, height = (800, 800)
road_w = int(width/1.6)
road_mark_w = int(width/80)
right_lane = width/2 + road_w/4
left_lane = width/2 - road_w/4

# animation parameters
speed = 1
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Rk's car game")
screen.fill((60, 220, 0))

# load images
car = pygame.image.load('cargame/car.png')
car_loc = car.get_rect()
car_loc.center = right_lane, height*0.8

# load car_enemy images
car2 = pygame.image.load('cargame/otherCar.png')
car2_loc = car2.get_rect()
car2_loc.center = left_lane, height*0.2

# main game loop
running = True
counter = 0
while running:
    counter += 1
    # increase game difficulty overtime
    if counter == 5000:
        speed += 0.15
        counter = 0
        print("level up", speed)
    # animate enemy vehicle
    car2_loc[1] += speed
    if car2_loc[1] > height:
        # randomly select lane
        if random.randint(0, 1) == 0:
            car2_loc.center = right_lane, -200
        else:
            car2_loc.center = left_lane, -200

    # end game logic
    if car_loc[0] == car2_loc[0] and car2_loc[1] > car_loc[1] - 250:
        print("GAME OVER! YOU LOST!")
        break
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            # move user car to the left
            if event.key in [pygame.K_a, pygame.K_LEFT]:
                car_loc = car_loc.move([-int(road_w/2), 0])
            # move user car to the right
            if event.key in [pygame.K_d, pygame.K_RIGHT]:
                car_loc = car_loc.move([int(road_w/2), 0])

    pygame.draw.rect(
     screen,
     (50, 50, 50),
     ((width/2-road_w/2), 0, road_w, height)
    )
    pygame.draw.rect(
        screen,
        (255, 240, 60),
        ((width/2-road_mark_w/2), 0, road_mark_w, height)
    )
    pygame.draw.rect(
        screen,
        (255, 255, 255),
        ((width/2-road_w/2+road_mark_w*2), 0, road_mark_w, height)
    )
    pygame.draw.rect(
        screen,
        (255, 255, 255),
        ((width/2+road_w/2-road_mark_w*3), 0, road_mark_w, height)
    )

    screen.blit(car, car_loc)
    screen.blit(car2, car2_loc)
    pygame.display.update()

# End the game
pygame.quit()
